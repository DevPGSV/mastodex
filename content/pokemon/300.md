---
Categories: ["pokemon"]
Colour: "Pink"
Number: 300
Instance: "catgirl.science"
IsPleroma: true
Username: "katida"
TranslationID: "PKMN_300"
Weight: 3000
---