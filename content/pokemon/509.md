---
Categories: ["pokemon"]
Colour: "Purple"
Number: 509
Instance: "pleroma.ombreport.info"
IsPleroma: true
Username: "nemesis"
TranslationID: "PKMN_509"
Weight: 5090
---