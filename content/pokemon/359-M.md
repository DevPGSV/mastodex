---
Categories: ["pokemon"]
Colour: "White"
Number: 359
Instance: "meemu.org"
Username: "whiiskers"
Mega: "M"
TranslationID: "PKMN_359"
Weight: 3591
---