var languageSwitcher = document.querySelector("#language-switcher")

languageSwitcher.addEventListener("change", function (event) {
    document.location.replace(baseURL + event.target.value)
})

var CLAIM_MODES = {
    ALL: 0,
    CLAIMED: 1,
    UNCLAIMED: 2
}
var claimMode = CLAIM_MODES.ALL
var textFilter = ""

function checkClaimMode (item) {
    var claimed = !item.classList.contains("pokemon-item-unclaimed")
    if (claimed && claimMode === CLAIM_MODES.UNCLAIMED) return false
    if (!claimed && claimMode === CLAIM_MODES.CLAIMED) return false
    return true
}

function checkTextFilter (item) {
    var pokemon = item.querySelector(".pokemon-name").innerText.toLowerCase()
    var owner = item.querySelector(".pokemon-owner a").innerText.toLowerCase()
    return pokemon.indexOf(textFilter) !== -1 ||  owner.indexOf(textFilter) !== -1
}

function filterPokemon () {
    var pokemon = [].slice.call(document.querySelector(".pokemon-list").children)
    pokemon.forEach(function (item, index) {
        var number = index + 1

        if (checkClaimMode(item) && checkTextFilter(item)) {
            item.classList.remove("hidden")
            return
        }
        item.classList.add("hidden")
    })
}
filterPokemon()

function setClaimFilter (radio) {
    claimMode = +radio.value
    filterPokemon()
}

function setTextFilter (textbox) {
    textFilter = textbox.value.toLowerCase()
    filterPokemon()
}

function toggleFooterOpen () {
    document.querySelector("footer").classList.toggle("open")
}

// We listen to the resize event
function resize () {
    document.documentElement.style.setProperty('--vh', `${window.innerHeight * 0.01}px`)
}
window.addEventListener('resize', resize)
resize()