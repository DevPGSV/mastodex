# Mastodex

Mastodex is a public listing of Mastodon users in which they are tied to a specific Pokémon of their choosing.

## Contributing

Currently, you can help in the following ways:
- Reporting bugs in the issue tracker.
- Providing new translations.
- Adding new features.

## Development

In order to develop Mastodex, you must install [Hugo](https://gohugo.io/). This project is a standard Hugo project, so learning Hugo will give you all the knowledge you require to develop for it.

## License

Mastodex is licensed under the Zlib license, and is copyright of Josef Frank and all contributors listed in the  [AUTHORS.md](AUTHORS.md) file.